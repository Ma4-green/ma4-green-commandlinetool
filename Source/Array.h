//
//  Array.h
//  CommandLineTool
//
//  Created by Matthew Green on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <iostream>


class Array
{
public:
    
    Array();
    ~Array();
    void add (float itemValue);
    float get (int index);
    int size();
    
private:
    
    float *arrayPointer;
    int arraySize;
};

#endif /* defined(__CommandLineTool__Array__) */
